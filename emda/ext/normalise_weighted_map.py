import fcodes_fast as fc
import numpy as np

def f_normalised_full_from_half(f1, f2, bin_idx):
    """ 
    Compute normalised Fourier coefficients from half data

    Args:
        f1, f2 (numpy.ndarray, complex): 3D numpy arrays of Complex type.
            They are the Fourier coefficients of half1 and half2 maps.
        bin_idx (numpy.ndarray, int): 3D numpy array of grid points.
        
    Returns:
        e0 (numpy.ndarray, complex): 3D numpy array of normalised Fourier
            coefficients of the full map.
        binfsc (numpy array, float): 1D numpy array of halfmap FSC.
    """
    assert f1.shape == f2.shape
    nx, ny, nz = f1.shape
    nbin = np.amax(bin_idx) + 1

    _,e1,_,_,_,halffsc1,bincount1 = fc.calc_fsc_using_halfmaps(
        f1, f2, bin_idx, nbin, 0, nx, ny, nz
    )

    return e1, halffsc1

def validate_fsc(res_arr, fsc, threshold=0.01):
    # Find the first index where FSC drops below the threshold
    threshold_index = np.argmax(fsc < threshold)

    # Set FSC values to zero from the threshold index onwards
    fsc_corrected = fsc.copy()
    fsc_corrected[threshold_index:] = 0

    return fsc_corrected

def read1dto3d(fsc1d, bin_idx):
    nbin = np.amax(bin_idx) + 1
    nx, ny, nz = bin_idx.shape
    fsc_grid = fc.read_into_grid(bin_idx, fsc1d, nbin, nx, ny, nz)
    return fsc_grid

def f_normalised_weighted_map(f1, f2, bin_idx, res_arr, weights=""):
    # Calculate normalised Fourier coeff. and half-FSC
    e1, halffsc1 = f_normalised_full_from_half(f1, f2, bin_idx)
    # Validate halffsc
    halffsc1_validated = validate_fsc(res_arr, halffsc1)

    # Set a weighting scheme
    if weights == "halfmapfsc":
        weights1d = halffsc1_validated
    elif weights == "bestmapfsc":
        weights1d = np.sqrt(2 * halffsc1_validated / (1 + halffsc1_validated))
    else:
        # Default: fullmapfsc
        weights1d = 2 * halffsc1_validated / (1 + halffsc1_validated)

    weights3d = read1dto3d(weights1d, bin_idx)
     
    return weights3d * e1